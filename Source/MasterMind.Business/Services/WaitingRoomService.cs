﻿using System;
using MasterMind.Business.Models;
using MasterMind.Data.DomainClasses;
using MasterMind.Data.Repositories;
using System.Collections.Generic;
using System.Linq;
using MasterMind.Data;

namespace MasterMind.Business.Services
{
    public class WaitingRoomService : IWaitingRoomService
    {
		/* Repository with all the waitingRooms */
		public IWaitingRoomRepository waitingRoomRepo { get; set; }

		/* Initializing the repository */
		public WaitingRoomService(IWaitingRoomRepository waitingRoomRepository)
        {
			waitingRoomRepo = waitingRoomRepository;
        }

		/* Filters the rooms so full rooms don't return */
        public ICollection<WaitingRoom> GetAllAvailableRooms()
        {
			/* Creating a new list of waitingrooms */
			ICollection<WaitingRoom> filteredListOfRooms = new List<WaitingRoom>();

			foreach (var item in waitingRoomRepo.GetAll())
			{
				/* Checks if the waitingroom has a spot free */
				if (item.Users.Count != item.MaximumAmountOfUsers)
				{
					filteredListOfRooms.Add(item);
				}
			}
			return filteredListOfRooms;
        }

		/* Creates a room and adds it into the repository */
        public WaitingRoom CreateRoom(WaitingRoomCreationModel roomToCreate, User creator)
        {
			/* Gets all the current waitingrooms and loops through all of them*/
			List<WaitingRoom> allRooms = waitingRoomRepo.GetAll() as List<WaitingRoom>;
			foreach (var item in allRooms)
			{
				/* If a waitingroom with that name already exists the program will throw an Exception */
				if (item.Name == roomToCreate.Name)
				{
					throw new ApplicationException("Room with this name already exists");
				}
			}
			/* Creation of a new waitingroom */
			WaitingRoom room = new WaitingRoom(roomToCreate.Name, creator, roomToCreate.GameSettings);
			/* Adding the new room to the repository */
			waitingRoomRepo.Add(room);
			return room;
        }

		/* Returns the waitroom with the specified ID */
        public WaitingRoom GetRoomById(Guid id)
        {
			return waitingRoomRepo.GetById(id);
        }

		/* Tries to join the room, if it fails it will also return the reason why it failed */
        public bool TryJoinRoom(Guid roomId, User user, out string failureReason)
        {
			/* If there is no room waitingRoomRepo will throw the DataNotFoundException */
			try
			{
				/* Tries to find the room */
				WaitingRoom room = waitingRoomRepo.GetById(roomId);
				/* Checks if the room is full */
				if (room.Users.Count < room.MaximumAmountOfUsers)
				{
					/* Checks if the user already is in the room */
					if (room.Users.Contains(user))
					{
						failureReason = "User already exists";
						return false;
					}
					/* Joining has been successful */
					room.Users.Add(user);
					failureReason = "";
					return true;
				}
				else
				{
					failureReason = "Room is full";
					return false;
				}
			}
			catch (DataNotFoundException)
			{
				failureReason = "Room not found";
				return false;
				throw;
			}
        }

		/* User tries to leave the waitingroom */
        public bool TryLeaveRoom(Guid roomId, User user, out string failureReason)
        {
			try
			{
				/* If the room is not found the waitingRoomRepo will throw a DataNotFoundException */
				WaitingRoom selectedRoom = waitingRoomRepo.GetById(roomId);

				/* Deletes the waitingroom if the person leaving is the creator */
				if (selectedRoom.CreatorUserId == user.Id)
				{
					waitingRoomRepo.DeleteById(roomId);
					failureReason = "";
					return true;
				}

				/* Person is not creation, checks if the user is in the room */
				else
				{
					bool userDoesNotExist = true;
					User selectedUser = null;
					foreach (var u in selectedRoom.Users)
					{
						if (u.Id == user.Id)
						{
							userDoesNotExist = false;
							selectedUser = u;
						}
					}
					if (userDoesNotExist)
					{
						failureReason = "User does not exists in the given room";
						return false;
					}
					/* Removes person from the selected waitingroom */
					selectedRoom.Users.Remove(selectedUser);
					failureReason = "";
					return true;
				}



			}
			catch (DataNotFoundException)
			{
				failureReason = "Data not found";
				return false;
				throw;
			}
        }
    }
}