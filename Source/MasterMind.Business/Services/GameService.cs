﻿using MasterMind.Data.DomainClasses;
using System;
using System.Linq;
using System.Collections.Generic;
using MasterMind.Data.Repositories;

namespace MasterMind.Business.Services
{
    public class GameService : IGameService
    {
		/* Repository of games */
        public IGameRepository repoGame { get; set; }

		/* Waitingroom Service */
        public IWaitingRoomService serviceWaiting { get; set; }

		/* Initializing the repository and the service */
        public GameService(IWaitingRoomService waitingRoomService, IGameRepository gameRepository)
        {
            repoGame = gameRepository;
            serviceWaiting = waitingRoomService;
        }

		/* Starts the game for a specified waitingroom */
        public IGame StartGameForRoom(Guid roomId)
        {
			/* Chooses the room with the specified ID */
                WaitingRoom room = serviceWaiting.GetRoomById(roomId);
			/* Checks if there are enough players in the waitingroom */
			if (room.Users.Count < 2)
			{
				throw new ApplicationException("Not enough players are in the room");
			}
			else
			{
				/* Creates a new List of players */
				IList<IPlayer> players = new List<IPlayer>();
				
				/* Adds all players inside of the list */
				foreach (var item in room.Users)
				{
					players.Add(item);
				}

				/* NOTE : You cannot convert the users of the room to replace the list 'players' */
				Game game = new Game(room.GameSettings, players);
				/* Adds the game to the repository */
				repoGame.Add(game);
				
				/* Tests specified that the ID has to be set after adding it to the repository */
				room.GameId = game.Id; 
				return game;
			}
        }

        public IGame GetGameById(Guid id)
        {
			/* Returns the game with the specified ID */
            IGame game = repoGame.GetById(id);
            return game;
        }

        public CanGuessResult CanGuessCode(Guid gameId, IPlayer player, int roundNumber)
        {
            /* Returns if the player is allowed to guess or not */
            IGame game = repoGame.GetById(gameId);
            return game.CanGuessCode(player, roundNumber);
        }

        public GuessResult GuessCode(Guid gameId, string[] colors, IPlayer player)
        {
			/* Finds the right game */
			IGame game = repoGame.GetById(gameId);

			/* If the player is not allowed to guess then the program will throw an ApplicationException */
			if (game.CanGuessCode(player, game.CurrentRound) == CanGuessResult.Ok)
			{
				return game.GuessCode(colors, player);
			}
			else
			{
				throw new ApplicationException("You are not allowed!");
			}
		}

		/* Returns the status of the game with the specified ID */
        public GameStatus GetGameStatus(Guid gameId)
        {
			return repoGame.GetById(gameId).GetStatus();
        }
    }
}