﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using MasterMind.Data.DomainClasses;


namespace MasterMind.Data.Repositories
{
    public class InMemoryWaitingRoomRepository : IWaitingRoomRepository
    {
		/* Dictionary with all the waitingrooms and their ID's */
		public ConcurrentDictionary<Guid, WaitingRoom> waitingRoomDict { get; set; }

		/* Initiliazing the dictionary */
		public InMemoryWaitingRoomRepository()
		{
			waitingRoomDict = new ConcurrentDictionary<Guid, WaitingRoom>();
		}

		/* Add's the waitingroom to the dictionary */	
        public WaitingRoom Add(WaitingRoom newWaitingRoom)
        {
			/* Creating a new unique ID for the waitingroom */
			newWaitingRoom.Id = Guid.NewGuid();
			/* Adding the waitroom with the GUID to the dictionary */
			waitingRoomDict.TryAdd(newWaitingRoom.Id, newWaitingRoom);
			return newWaitingRoom;
        }

		/* Returns the waitingroom with the specified ID */
        public WaitingRoom GetById(Guid id)
        {
			if (waitingRoomDict.ContainsKey(id))
			{
				return waitingRoomDict[id];
			}
			/* When no room has been found the program throw this Exception */
			throw new DataNotFoundException();
        }

		/* Returns all rooms in the dictionary */
        public ICollection<WaitingRoom> GetAll()
        {
			return new List<WaitingRoom>(waitingRoomDict.Values);
        }

		/* Deletes the waitingroom with the specified ID, when no ID has been found the program does nothing */
        public void DeleteById(Guid id)
        {
			if (waitingRoomDict.ContainsKey(id))
			{
				WaitingRoom w;
				waitingRoomDict.Remove(id, out w);
			}
        }
    }
}