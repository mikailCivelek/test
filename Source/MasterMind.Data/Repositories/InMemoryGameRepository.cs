﻿using System;
using System.Collections.Concurrent;
using MasterMind.Data.DomainClasses;

namespace MasterMind.Data.Repositories
{
    public class InMemoryGameRepository : IGameRepository
    {
		/* Dictionarry with all the games in it */
		public ConcurrentDictionary<Guid, IGame> games { get; set; }

		/* Initializing of the dictionary */
		public InMemoryGameRepository()
		{
			games = new ConcurrentDictionary<Guid, IGame>();
		}

		/* Adding a new game to the dictionary */
        public IGame Add(IGame newGame)
        {
			/* Giving the new game a new unique ID */
			newGame.Id = Guid.NewGuid();
			games.TryAdd(newGame.Id, newGame);
			return newGame;
        }

		/* Returns the game with the specified ID */
        public IGame GetById(Guid id)
        {
			if (games.ContainsKey(id)){
				return games[id];
			}
		/* when no game has been found the program returns this exception */
            throw new DataNotFoundException();
        }

		/* Removes a room from the dictionary with the specified ID */
        public void DeleteById(Guid id)
        {
			if (games.ContainsKey(id))
			{
				/* This variable has to be there because of the TryRemove() method */
				IGame g;
				games.TryRemove(id, out g);
			}
		}
    }
}