﻿using System;
using System.Collections.Generic;

namespace MasterMind.Data.DomainClasses
{
    public class WaitingRoom
    {
        public const int DefaultMaximumAmountOfUsers = 4;

        public WaitingRoom(string name, User creator, GameSettings gameSettings)
        {
			/* Creating a new unique ID for the room */
			Id = Guid.NewGuid();
			Name = name;
			CreatorUserId = creator.Id;
			/* initializing the list of users */
			Users = new List<User>();
			/* adding the creator to the users list */
			Users.Add(creator);
			/* importing the gamesettings */
			GameSettings = gameSettings;
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public IList<User> Users { get; set; }

        public Guid CreatorUserId { get; }

        public GameSettings GameSettings { get; set; }

        public Guid GameId { get; set; }

        public virtual int MaximumAmountOfUsers => DefaultMaximumAmountOfUsers;

        public bool GameHasStarted => GameId != Guid.Empty;
    }
}