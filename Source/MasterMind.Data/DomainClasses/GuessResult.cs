﻿using System.Collections.Generic;
using System.Linq;

namespace MasterMind.Data.DomainClasses
{
    public class GuessResult
    {
		/* Amount of colors that have been guessed right with the right position by the player */
        private int _correctColorAndPositionAmount;
		/* Amount of colors that have been guessed right with the wrong position by the player */
        private int _correctColorAmount;

        public string[] Colors { get; set; }

        public virtual int CorrectColorAndPositionAmount => _correctColorAndPositionAmount; //must be virtual for some automated test to work!

        public virtual int CorrectColorAmount => _correctColorAmount; //must be virtual for some automated test to work!

        public GuessResult(string[] colors)
        {
			Colors = colors;
			_correctColorAndPositionAmount = 0;
			_correctColorAmount = 0;
        }

        public void Verify(string[] codeToGuess)
        {

			List<string> copyColors = new List<string>();
			List<string> copyCodeToGuess = new List<string>();


			for (int i = 0; i < Colors.Length; i++)
			{
				if (codeToGuess[i] == Colors[i])
				{
					_correctColorAndPositionAmount++;
				}
				else
				{
					copyColors.Add(Colors[i]);
					copyCodeToGuess.Add(codeToGuess[i]);
				}
			}

			for (int i = 0; i < copyCodeToGuess.Count; i++)
			{
				if (copyCodeToGuess.Contains(copyColors[i]))
				{
					int index = copyCodeToGuess.IndexOf(copyColors[i]);
					copyColors[i] = null;
					copyCodeToGuess[index] = null;
					_correctColorAmount++;
				}
			}




			///* List of colours that have already been chosen */
			//List<string> chosenColors = new List<string>();

			///* Looking for colors that have the right color and right position and adding those to the list */
			//for (int i = 0; i < codeToGuess.Length; i++)
			//{
			//	if (codeToGuess[i] == Colors[i])
			//	{


			//		/* OG */
			//		/* Let's not fill our list with duplicates */
			//		if (!chosenColors.Contains(codeToGuess[i]))
			//		{
			//			chosenColors.Add(codeToGuess[i]);
			//		}
			//		_correctColorAndPositionAmount++;
			//	}
			//}

			///* Goes through each color in the guess and checks if the color is in the code that needs to be guessed .
			// * If the color is not in the list of 'chosenColors' then we can add 1 to the _correctColorAmount.
			// * If the color already exists in the list then we will do nothing.
			// * This is so the game still functions correctly even you have duplicates */
			//foreach (var s in codeToGuess)
			//{
			//	if (Colors.Contains(s))
			//	{
			//		_correctColorAmount++;
			//		//if (!chosenColors.Contains(s))
			//		//{
			//		//	_correctColorAmount++;
			//		//}
			//	}
			//}

			//Dictionary<string, int> test = new Dictionary<string, int>();
			//         test.Add("red", 0);
			//         test.Add("orange", 0);
			//         test.Add("yellow", 0);
			//         test.Add("green", 0);
			//         test.Add("blue", 0);
			//         test.Add("purple", 0);
			//         test.Add("black", 0);
			//         test.Add("white", 0);


			//         for (int i = 0; i < codeToGuess.Length; i++)
			//         {
			//             test[codeToGuess[i]] += 1;
			//         }

			//         for (int i = 0; i < codeToGuess.Length; i++)
			//         {
			//             if(test[Colors[i]] != 0)
			//             {
			//                 _correctColorAmount++;
			//                 test[Colors[i]]--;
			//             }
			//                 if (Colors[i] == codeToGuess[i])
			//                 {
			//                     _correctColorAndPositionAmount++;
			//                 }
			//         }
			//         _correctColorAmount = _correctColorAmount - _correctColorAndPositionAmount;
		}
    }
}