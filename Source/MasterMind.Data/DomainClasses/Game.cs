using System;
using System.Collections.Generic;
using System.Linq;

namespace MasterMind.Data.DomainClasses
{
  public class Game : IGame
  {

    private int _currentRound; //private needed appearantly

    Dictionary<string[], IPlayer> PlayerHistory; //dictionary that keeps as key the guess, value player
    public Guid Id { get; set; }
    public GameSettings Settings { get; set; } //game settings
    public IList<IPlayer> Players { get; } //list of players 
    public string[] PossibleColors { get; }
    public int CurrentRound => _currentRound; //inherited 
    private string[] _codeToGuess; // correct code to guess
    GuessResult correct; //correct code with variable in it 
    GameStatus status; //status of the game

    /// <summary>
    /// Constructs a Game object and generates a code to guess.
    /// </summary>
    public Game(GameSettings settings, IList<IPlayer> players)
    {

      Id = Guid.NewGuid();
      Settings = settings;
      Players = players;
      _currentRound = 1;
      PossibleColors = new string[Settings.AmountOfColors];
      fillColors();
      _codeToGuess = GenerateCode(Settings.CodeLength, Settings.DuplicateColorsAllowed);
      correct = new GuessResult(_codeToGuess);
      status = new GameStatus();
      status.RoundWinners = new IPlayer[3];
      PlayerHistory = new Dictionary<string[], IPlayer>();

    }
    public string[] GenerateCode(int length, Boolean duplicates) //CODE GENERATOR CORRECT
    {
      //RANDOM CODE GENERATOR CAN BE POSSIBLY MADE EASIER
      Random rnd = new Random();
      string[] code = new string[length];
      int number = 0;
      if (duplicates) //DUPLICATES ALLOWED
      {
        for (int i = 0; i < length; i++)
        {
          number = rnd.Next(0, length);
          code[i] = PossibleColors[number];
        }
      }
      else //DUPLICATES DENIED
      {
        List<int> used = new List<int>();

        for (int i = 0; i < length; i++)
        {
          do
          {
            number = rnd.Next(0, length);
          } while (used.Contains(number));
          used.Add(number);
          code[i] = PossibleColors[number];
        }
      }

      return code;
    }
    private void fillColors()                   //8 COLORS! DEFINED IN GameSettings.cs
    {
      List<string> colors = new List<string>();
      colors.Add("#d54c4c");//HEX FOR RED
      colors.Add("#9ac8db");//BLUE
      colors.Add("#f5f53a");//YELLOW
      colors.Add("#bcff48");//GREEN
      colors.Add("#ffad5a");//ORANGE
      colors.Add("#d700d7");//PURPLE
      colors.Add("#202020");//BLACK
      colors.Add("#FFFFFF");//WHITE
      for (int i = 0; i < Settings.AmountOfColors; i++)
      {
        PossibleColors[i] = colors[i];
      }
    }

    public CanGuessResult CanGuessCode(IPlayer player, int roundNumber)//checks if the user is able to make a guess or not
    {

      if (_currentRound < roundNumber) // round isn't started
      {
        return CanGuessResult.RoundNotStarted;
      }
      else if (_currentRound > roundNumber) // round is going to next
      {
        return CanGuessResult.NotAllowedDueToGameStatus;
      }
      Dictionary<Guid, int> turns = new Dictionary<Guid, int>(); // Dictionary that tracks each player how many guesses they made
      turns = PlayerHistory.GroupBy(x => x.Value.Id).ToDictionary(y => y.Key, y => y.Count());
      if (turns.ContainsKey(player.Id))
      {
        if (turns[player.Id] >= Settings.MaximumAmountOfGuesses) // player turns are at max
        {
          return CanGuessResult.MaximumReached; // 
        }
        foreach (var t in turns)
        {
          if (t.Key != player.Id)
          {
            if (t.Value < turns[player.Id])
            {
              return CanGuessResult.MustWaitOnOthers;
            }
          }
        }
        if (turns.Count() != Players.Count())
        {
          return CanGuessResult.MustWaitOnOthers;
        }
      }
      return CanGuessResult.Ok;
    }



    //function to guess a code//
    public GuessResult GuessCode(string[] colors, IPlayer player)
    {
        correct.Verify(colors); //how much of your code is correct
        PlayerHistory.Add(colors, player);
        if (correct.CorrectColorAndPositionAmount == Settings.AmountOfColors) // if you got it correct
        {
          status.RoundWinners[_currentRound - 1] = player; //add roundwinner
          status.GameOver = true;
          _currentRound++;
          status.CurrentRoundNumber++;
          PlayerHistory.Clear();
        }
        if (PlayerHistory.Count == Settings.MaximumAmountOfGuesses * Players.Count) //end of round, everybody is out of turns
        {
          _currentRound++;
          PlayerHistory.Clear();
        }
      return correct;
    }
    public GameStatus GetStatus()
    {
      if (Settings.AmountOfRounds == 1) //default game mode is single round game
      {
        if (status.RoundWinners != null)
        {
          status.FinalWinner = status.RoundWinners[0]; //roundwinner will be finalwinner
        }
      }
      return status;
    }
  }
}
